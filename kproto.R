library(clustMixType)
library(fpc)
#library(ggforce)
#library(concaveman)
library(factoextra)
library(rgl)
library(cluster)
library(cba)
#library(heatmaply)

#LOOKING AT THE WHOLE DT WE CAN SEE ENERGY AND LOUDNESS HAVE A STRONG CORRELATION(0.8), 
#LOUDNESS AND ACOUSTICNESS A STRONG NEGATIVE ONE(-0.75)
#DANCE AND VALENCE (HOW HAPPY IT SOUNDS) HAVE A MODERATE POSITIVE CORRELATION(0.56)
#ENERGY AND ACOUSTICNESS HAVE A STRONG NEGATIVE ONE(-0.8)
#BASED ON THIS ENERGY AND LOUDNESS ARE SIMILAR IN NATURE SO I DECIDED TO GET RID OF LOUDNESS AND ACOUSTICNESS
#THE HEATMAP SHOWS THE CORRELATIONS
#heatmaply_cor(cor(my_songs[,5:15]))

#SCALING IMPORTANT COLUMNS IN MY_SONGS #MIGHT HAVE TO RETURN ACOUSTICNESS FOR IT TO DIVIDE CLASSICAL WELL
my_songs_scaled <- as.data.table(my_songs[, c(6,7,8,9,12)]) %>% scale()
#cbind data.frame lets genre stay as factor and the others as numberic
my_songs_scaled <- cbind.data.frame(my_songs_scaled, genre = my_songs$genre1)
#                                           as.numeric

#THIS FOLLOWING ROW GETS THE CLUSTERING TENDENCY OF THE DATA SET. IT ACTS AS A HYPOTHESIS TEST WHERE H0 := DATA IS RANDOMLY
#DISTRIBUTED. A VALUE CLOSE TO 1 MEANS THE DATA IS HIGHLY CLUSTERED, THIS FUNCTION RETURNS ~0.76 ON EVERY RUN WITH ANY SIZED
#INPUT SO OUR DATA SEEMS TO BE CLUSTERABLE.
#hopkins <- get_clust_tendency(my_songs_scaled, 100)
#hopkins$hopkins_stat
#THIS PLOT SHOWS A DISSIMILARITY MATRIX OF THE WHOLE DATASET WITH VAT ALGORITHM
#hopkins$plot
#ELBOW METHOD FOR FINDING THE OPTIMAL NUMBER OF CLUSTERS
#set.seed(123)
#COMPUTE AND PLOT WSS FOR K = 2 TO K = 15. (WSS = WITHIN-CLUSTERS SUM OF SQUARES)
#k.max <- 15
#wss <- sapply(1:k.max, function(k){kproto(as.data.frame(my_songs_scaled), k)$tot.withinss})
#THIS PLOTS THE WITHIN-CLUSTER DISTANCE FROM THE CENTERS, AND SHOWS IT'S SLOW DEGRADATION, SHOWING THE OPTIMAL VALUE
#FOR CLUSTERS WITH MINIMAL COMPUTING COSTS
#wss
#plot(1:k.max, wss,
#     type="b", pch = 19, frame = FALSE, 
#     xlab="Number of clusters K",
#     ylab="Total within-clusters sum of squares")
#AS YOU CAN SEE IN X AT 7 CLUSTERS THE FUNCTION DROPS A BIGGER CHUNK BUT AFTER THAT IT STAGNATES,
#7 CLUSTERS SHOULD BE THE BEST(?)
#x <- lapply(seq_along(wss[1:14]), function(i){ paste(wss[[i]]-wss[[i+1]])})

#K-PROTOTYPES CLUSTERING OF THE SONGS
#clusters2 <- kproto(my_songs_scaled, 4)
clusters <- kproto(my_songs_scaled, 3, lambda=2, nstart=10)
#COLLECTING THE CENTERS OF EACH CLUSTER
centers <- clusters$centers
#"INVESTIGATION OF VARIANCES TO SPECIFY LAMBDA FOR K-PROTOTYPE CLUSTERING"
summary(clusters)
#COLLECTING EACH ELEMENT'S DISTANCES FROM THE CENTERS OF EACH CLUSTER
dist_mx <- clusters$dists
#BINDING THE CHOSEN CLUSTER OF THE SONGS TO THE SCALED MY_SONGS DATA TABLE
clus_mx <- cbind(dist_mx, as.data.frame(clusters["cluster"]["cluster"]))
my_songs_scaled <- cbind(my_songs_scaled, as.data.table(clusters["cluster"]))
#AND LATER MAYBE BINDING IT TO THE ORIGINAL ONE
my_songs <- cbind(my_songs, as.data.table(clusters["cluster"]["cluster"]))
my_songs <- my_songs[,c(1,2,3,4,5,16,6,7,8,9,10,11,12,13,14,15)]

#SELECTING THE DISTANCE-MX OF THE VARIABLES FROM THE CLUSTER LIST, AND PLOTTING IT
dist_mx <- dist(clusters$dists)
#fviz_dist(clus_mx)
#MELTING IT INTO LONG FORMAT
#dist_mx <- melt(as.matrix(dist_mx))

#PRETTY NICE 3D PLOT OF THE SONGS COLORED BY CLUSTERS
with(my_songs_scaled[1:1000,], plot3d(valence, popularity, genre, 
                                     type="s", col=cluster))

#THIS SHOWS THE CLUSTERS IN BOX PLOTS
#dev.new(width=5,height=4,noRStudioGD = TRUE)
#clprofiles(clusters, my_songs_scaled)

#THIS SHOWS CLUSTERS IN A 2D SETTING, BUT THEY OVERLAP SO IT'S CONFUSING
ggplot(my_songs_scaled, aes(x = popularity, y = valence)) +
  geom_mark_hull(expand=0,aes(fill=factor(cluster))) +
  geom_point() + theme_gray()

#THIS SHOWS A MORE DETAILED ANALYSIS OF THE CLUSTERS
cluster_data <- cluster.stats(dist_mx, clusters$cluster)
sep_mx <- cluster_data$separation.matrix

#THIS SHOWS A SILHOUETTE ANALYSIS OF THE CLUSTERS
sil = silhouette(clusters$cluster, dist_mx)
windows()
plot(sil)
mean(sil[,3])
