library(clustMixType)
library(fpc)
library(factoextra)
library(cluster)
library(dendextend)
library(rgl)

#SCALING IMPORTANT COLUMNS IN MY_SONGS #MIGHT HAVE TO RETURN ACOUSTICNESS FOR IT TO DIVIDE CLASSICAL WELL
my_songs_scaled <- as.data.table(my_songs[, c(6,7,8,9,12)]) %>% scale()
#cbind data.frame lets genre stay as factor and the others as numberic
my_songs_scaled <- cbind.data.frame(my_songs_scaled, genre = my_songs$genre1)

#creating a small pool of data for plotting (it was randomized earlier so should be mostly fine)
my_songs_small <- data.table(my_songs_scaled[0])
#creating a small pool of data from the main table, 10 of "others", 50 of everything else
for(i in seq_along(main_genre_names$genre1)){
#the "other" genre has only 27 songs, so only 10 should be in a small dataset
  if(i == 7){
    my_songs_small <- bind_rows(my_songs_small, subset(my_songs_scaled, genre==i)[1:10,])
  }else{
    my_songs_small <- bind_rows(my_songs_small, subset(my_songs_scaled, genre==i)[1:50,])
  }
}
#shuffling the small data set
my_songs_small <- my_songs_small[sample(nrow(my_songs_small)),]

#USING THE DAISY ALGORITHM ON THE DATA, IT COMPUTES THE PAIRWISE DISSIMILARITIES BETWEEN OBSERVATIONS IN THE
#DATASET WITH THE GOWER METRIC, AFTER THAT WE HAVE TO PASS IT TO THE HCLUST FUNCTION FOR IT TO MAKE
#A HIEARCHICAL CLUSTERING OF THE DATA, IT SHOWS A REALLY DEEP CLUSTERING AND WE CUT IT OFF AT 7
clusters <- daisy(my_songs_scaled)
#x <- as.matrix(clusters)
summary(clusters)
#dist_mx <- dist(clusters)
clusters2 <- hclust(clusters, method = "complete")

#CUTTING THE DENDORGRAM AT THE 
clusters3 <- as.data.table(cutree(clusters2, 7))
#PLOTTING IT AS DENDROGRAMS
dg <- as.dendrogram(clusters2)

dg.col <- dg %>%
  set("branches_k_color", k = 7, value =   c("darkslategray", "darkslategray4", "darkslategray3", "gold3", "darkcyan", "cyan3", "gold3")) %>%
  set("branches_lwd", 0.6) %>%
  set("labels_colors", 
      value = c("darkslategray")) %>% 
  set("labels_cex", 0.5)

ggd1 <- as.ggdend(dg.col)
#NORMAL DENDROGRAM PLOT
ggplot(ggd1, theme = theme_minimal()) + labs(x = "Num. observations", y = "Height", title = "Dendrogram, k = 7")
#CIRCULAR DENDROGRAM PLOT
ggplot(ggd1, labels = T) + 
  scale_y_reverse(expand = c(0.2, 0)) +
  coord_polar(theta="x")

#BINDING THE CLUSTER TO THE MAIN DATA
my_songs_scaled <- cbind.data.frame(my_songs_scaled, cluster = clusters3)

#PRETTY NICE 3D PLOT OF THE SONGS COLORED BY CLUSTERS
with(my_songs_scaled, plot3d(genre, popularity, dance, 
                                      type="s", col=V1))

#WITH 4 CLUSTERS CUT OFF SHOWING ALL SUMS WE CAN SEE THAT THE FIRST CLUSTER CONSISTS OF HIP HOP, METAL, R&B, DANCE AND ROCK
#THE SECOND ONE CONSISTS OF POP SONGS AND "OTHER" SONGS, THE THIRD ONE CONSISTS OF JAZZ AND FOLK, 
#AND THE FORTH ONE MOSTLY CONSISTS OF CLASSICAL AND SOME OTHERS
summary(subset(my_songs_scaled, V1 == 7))
#WITH 7 CLUSTERS MOST OF THE SONGS ARE CLUSTERED INTO THEIR OWN CLUSTERS BY THEIR GENRE